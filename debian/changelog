pocof1-support (0.2.4) unstable; urgency=medium

  * d/control: move to 6.1 kernel

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 16 Dec 2022 12:19:13 +0100

pocof1-support (0.2.3) unstable; urgency=medium

  * d/control: move to 6.0 kernel

 -- Arnaud Ferraris <aferraris@debian.org>  Fri, 28 Oct 2022 15:11:37 +0200

pocof1-support (0.2.2) unstable; urgency=medium

  * d/control: move to 5.19 kernel

 -- Arnaud Ferraris <aferraris@debian.org>  Sat, 01 Oct 2022 15:46:15 +0200

pocof1-support (0.2.1) unstable; urgency=medium

  * d/control: depend on `protection-domain-mapper`
    `pd-mapper` has been uploaded to Debian, with a change of the
    corresponding package name, let's pull that one instead.

 -- Arnaud Ferraris <aferraris@debian.org>  Mon, 08 Aug 2022 16:44:56 +0200

pocof1-support (0.2.0) unstable; urgency=medium

  * d/control: depend on mobile-initramfs-tools.
    Until now, `mobile-initramfs-tools` was pulled as a dependency to
    `mobian-tweaks-common`. As this is about to change, and this device
    still requires scripts from `mobile-initramfs-tools`, let's make it a
    direct dependency.
  * d/control: fix upgrade by declaring a conflict with old qrtr

 -- Arnaud Ferraris <aferraris@debian.org>  Tue, 19 Jul 2022 13:42:31 +0200

pocof1-support (0.1.7) unstable; urgency=medium

  * d/control: keep up with latest changes.
    Mobian repos have moved to salsa, so ensure all URLs point to the right
    location. Additionally, bump Standards-Version and update my email
    address.
  * d/control: depend on qrtr-tools
    `qrtr` has been uploaded to Debian, with a change of the corresponding
    binary package, let's pull that one instead.

 -- Arnaud Ferraris <aferraris@debian.org>  Mon, 18 Jul 2022 12:49:28 +0200

pocof1-support (0.1.6) unstable; urgency=medium

  * d/control: use 5.17 kernel

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sat, 09 Apr 2022 15:50:01 +0200

pocof1-support (0.1.5) unstable; urgency=medium

  * d/control: depend on newly introduced packages.
    The USB gadget config and scripts have moved from `mobian-tweaks-common`
    to `mobile-usb-networking`, and device-specific initramfs scripts are
    now part of `mobile-initramfs-tools`, so make sure we depend on those
    packages.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Thu, 24 Mar 2022 21:19:58 +0100

pocof1-support (0.1.4) unstable; urgency=medium

  * d/control: depend on libdrm-freedreno1

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Thu, 17 Mar 2022 10:44:41 +0100

pocof1-support (0.1.3) unstable; urgency=medium

  * debian: add gbp.conf
  * d/control: switch to 5.16 kernel

 -- Arnaud Ferraris <arnaud.ferraris@collabora.com>  Fri, 04 Mar 2022 13:27:12 +0100

pocof1-support (0.1.2) unstable; urgency=medium

  * d/control: switch to 5.14 kernel

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 25 Jul 2021 19:52:52 +0200

pocof1-support (0.1.1) unstable; urgency=medium

  * d/control: update dependencies.

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Fri, 16 Jul 2021 11:42:38 +0200

pocof1-support (0.1.0) unstable; urgency=medium

  * Initial release

 -- Arnaud Ferraris <arnaud.ferraris@gmail.com>  Sun, 02 May 2021 19:06:55 +0200
